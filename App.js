import React from "react";
import {StyleSheet, TouchableOpacity, Text, View} from "react-native";

async function CreateTicket() {
    try {
        let response = await fetch('https://dianabanu.zendesk.com/api/v2/tickets.json?async=true', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Basic YmFudW1hcmluYWRpYW5hQGdtYWlsLmNvbTpncmVldGFuZGNvZGU='
            },
            body: JSON.stringify({
                "ticket": {
                    "subject": "Ticket",
                    "comment": {"body": "Ticket description."},
                    "priority": "urgent"
                }
            }),
        });
        let responseJson = await response.json();
        console.log(responseJson);
        return responseJson;
    } catch (error) {
        console.error(error);
    }
}

const SendEmailButton = () => {
    return (
        <View style={styles.container}>
            <TouchableOpacity
                onPress={CreateTicket()}
            >
                <Text>Press me for a new ticket</Text>
            </TouchableOpacity>
        </View>
    );
};

export default SendEmailButton;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    }
});
